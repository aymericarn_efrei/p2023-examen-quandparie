﻿using Microsoft.AspNetCore.Mvc;
using QuandParie.Api.Contracts;
using QuandParie.Core.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuandParie.Api.Controllers
{
    [ApiController]
    public class WageringController: ControllerBase
    {
        private readonly WageringApplication application;

        public WageringController(WageringApplication application)
        {
            this.application = application;
        }

        [HttpPost("/customers/{email}/wagers")]
        public async Task<ActionResult<Core.Domain.Wager>> PlaceWager(string email, WagerCreation wager)
        {
            try 
            {
                return await application.PlaceWager(email, wager.Amount, wager.OddsIds);
            }
            catch (ArgumentException argumentException)
            {
                return BadRequest(argumentException.Message);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                return BadRequest(invalidOperationException.Message);
            }
        }

        [HttpGet("/customers/{email}/wagers")]
        public async Task<ActionResult<IReadOnlyList<Core.Domain.Wager>>> GetWagers(string email)
        {
            var result = await application.GetWagers(email);
            return result != null
                ? new ActionResult<IReadOnlyList<Core.Domain.Wager>>(result)
                : NotFound();
        }
    }
}
