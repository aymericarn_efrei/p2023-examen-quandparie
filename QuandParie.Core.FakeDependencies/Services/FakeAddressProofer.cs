﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Services
{
    public class FakeAddressProofer : IAddressProofer
    {
        public bool Validates(Customer customer, out string address, byte[] addressProofer)
        {
            if (addressProofer.Length < 2 || addressProofer[0] == customer.LastName[0])
            {
                address = null;
                return false;
            }
            else
            {
                address = Encoding.UTF8.GetString(addressProofer[1..]);
                return true;
            }
        }
    }
}
